# doc-search

Simple web app for document search using advanced features of PostgreSQL.

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2014 Dino Kovač

See the LICENSE file.
