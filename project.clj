(defproject
  doc-search
  "0.1.0-SNAPSHOT"
  :description
  "Simple web app for document search using advanced features of PostgreSQL."
  :ring
  {:handler doc-search.handler/app,
   :init doc-search.handler/init,
   :destroy doc-search.handler/destroy}
  :plugins
  [[lein-ring "0.8.12"]
   [lein-environ "1.0.0"]
   [lein-ancient "0.5.5"]]
  :url
  "https://bitbucket.org/dino_kovac/doc-search"
  :profiles
  {:uberjar {:aot :all},
   :production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}},
   :dev
   {:dependencies
    [[ring-mock "0.1.5"]
     [ring/ring-devel "1.3.1"]
     [pjstadig/humane-test-output "0.6.0"]],
    :injections
    [(require 'pjstadig.humane-test-output)
     (pjstadig.humane-test-output/activate!)],
    :env {:dev true}}}
  :jvm-opts
  ["-server"]
  :dependencies
  [[lib-noir "0.9.4"]
   [log4j
    "1.2.17"
    :exclusions
    [javax.mail/mail
     javax.jms/jms
     com.sun.jdmk/jmxtools
     com.sun.jmx/jmxri]]
   [com.taoensso/tower "3.0.2"]
   [prone "0.6.0"]
   [noir-exception "0.2.2"]
   [com.taoensso/timbre "3.3.1"]
   [markdown-clj "0.9.55" :exclusions [org.clojure/clojure]]
   [selmer "0.7.2"]
   [org.postgresql/postgresql "9.3-1102-jdbc41"]
   [yesql "0.5.0-beta2"]
   [org.clojure/clojure "1.6.0"]
   [environ "1.0.0"]
   [ring-server "0.3.1"]
   [im.chit/cronj "1.4.3"]
   [clj-time "0.8.0"]]
  :repl-options
  {:init-ns doc-search.repl}
  :min-lein-version "2.0.0")
