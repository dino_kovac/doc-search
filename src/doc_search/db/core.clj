(ns doc-search.db.core
  (:require [yesql.core :refer [defqueries]]
            [doc-search.util :as util]
            [clojure.string :as str]
            [clojure.java.jdbc :as jdbc]))

(def db-spec
  {:subprotocol "postgresql"
   :subname "//192.168.56.12/doc_search"
   :user "postgres"
   :password "reverse"})

(def search-format
  "SELECT id,
  ts_headline(content, to_tsquery('%1$s')),
  content,
  ts_rank(content_tsvector, to_tsquery('%1$s')) rank
  FROM documents
  WHERE %2$s
  ORDER BY rank DESC;")

(def report-hours-format
  "SELECT * FROM
  crosstab('
  SELECT query_string,
  CAST(EXTRACT(HOUR FROM queried_at) AS int) AS hour,
  COUNT(*) AS queryCount
  FROM queries
  WHERE queried_at BETWEEN ''%1$s''::timestamp AND ''%2$s''::timestamp
  GROUP BY query_string, hour
  ORDER BY query_string, hour',
  'SELECT hourOfDay FROM hours ORDER BY hourOfDay')
  AS pivotTable (
  query_string VARCHAR(200),
  h00_01 bigint,
  h01_02 bigint,
  h02_03 bigint,
  h03_04 bigint,
  h04_05 bigint,
  h05_06 bigint,
  h06_07 bigint,
  h07_08 bigint,
  h08_09 bigint,
  h09_10 bigint,
  h10_11 bigint,
  h11_12 bigint,
  h12_13 bigint,
  h13_14 bigint,
  h14_15 bigint,
  h15_16 bigint,
  h16_17 bigint,
  h17_18 bigint,
  h18_19 bigint,
  h19_20 bigint,
  h20_21 bigint,
  h21_22 bigint,
  h22_23 bigint,
  h23_00 bigint)
  ORDER BY query_string;")

(def report-days-format
  "SELECT * FROM
  crosstab('
  SELECT query_string,
  EXTRACT(YEAR FROM queried_at) || ''_''
  || EXTRACT(MONTH FROM queried_at) || ''_''
  || EXTRACT(DAY FROM queried_at) AS q_date,
  COUNT(*) AS queryCount
  FROM queries
  WHERE queried_at BETWEEN ''%1$s''::timestamp AND ''%2$s''::timestamp
  GROUP BY query_string, q_date
  ORDER BY query_string, q_date',
  'SELECT DISTINCT EXTRACT(YEAR FROM queried_at) || ''_''
  || EXTRACT(MONTH FROM queried_at) || ''_''
  || EXTRACT(DAY FROM queried_at) AS query_date
  FROM queries
  ORDER BY query_date')
  AS pivotTable (
  query_string VARCHAR(200),
  %3$s)
  ORDER BY query_string;")

(defqueries "doc_search/db/queries.sql"
  {:connection db-spec})

(defn raw-query [query]
  (jdbc/query db-spec [query]))

(defn init []
  (add-extensions!)
  (create-documents-table!)
  (create-queries-table!)
  (try
    (create-document-trigger!)
    (create-document-index!)
    (create-hours-table!)
    (catch Exception e nil))
  )

(declare exact-doc-query dict-doc-query fuzzy-doc-query headline-query)

(defn query-documents [q oper type]
  (let [tokens (util/tokenize q)
        _ (create-query! {:queried_at (util/current-timestamp)
                          :query_string (->>
                                         (map #(str "'" % "'") tokens)
                                         (str/join " & "))})
        headline-part (headline-query tokens oper)
        where-part (condp = type
                     "exact" (exact-doc-query tokens oper)
                     "dict" (dict-doc-query tokens oper)
                     "fuzzy" (fuzzy-doc-query tokens oper)
                     "unrecognized query type!")
        query (format search-format headline-part where-part)]
    [(raw-query query) query]))

(defn prepare-tsquery-words [words]
  (str/join " & " (str/split words #" ")))

(defn headline-query [tokens oper]
  (->>
   (map #(prepare-tsquery-words %) tokens)
   (map #(if (util/contains-char? % \&) (str "(" % ")") %))
   (str/join (if (= oper "and") " & " " | "))))

(defn exact-doc-query [tokens oper]
  (->>
   (map #(str "content LIKE '%" % "%'") tokens)
   (str/join (str " " (str/upper-case oper) " "))))

(defn dict-doc-query [tokens oper]
  (->>
   (map #(str "content_tsvector @@ to_tsquery('english','" (prepare-tsquery-words %) "')") tokens)
   (str/join (str " " (str/upper-case oper) " "))))

(defn fuzzy-doc-query [tokens oper]
  (->>
   (map #(str "content % '" % "'") tokens)
   (str/join (str " " (str/upper-case oper) " "))))

(defn get-query-report-hours [from to]
  (raw-query (format report-hours-format from to)))

(defn get-query-report-days [from to]
  (let [report-days (->> (get-report-days)
                         (map #(str "d" (:query_date %) " bigint")))
        column-string (str/join ", " report-days)]
    (raw-query (format report-days-format from to column-string))))

(defn get-query-report [from to granularity]
  (let [from-timestamp (util/start-of-day-timestamp (util/from-date-string from))
        to-timestamp (util/end-of-day-timestamp (util/from-date-string to))
        db-results (if (= granularity "hour")
                     (get-query-report-hours from-timestamp to-timestamp)
                     (get-query-report-days from-timestamp to-timestamp))
        results (map #(->> (into [] %)
                           (sort-by first)
                           (util/prepend-last)) db-results)
        columns (map #(name (first %)) (first results))
        res (map (fn [row] (map second row)) results)]
    [columns res]))
