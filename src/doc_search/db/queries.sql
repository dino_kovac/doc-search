-- name: add-extensions!
-- Adds required pg extensions.
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS tablefunc;

-- name: create-documents-table!
CREATE TABLE IF NOT EXISTS documents
(
  id serial,
  content text,
  content_tsvector tsvector
  );

-- name: create-document-trigger!
CREATE TRIGGER content_ins BEFORE INSERT ON documents FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('content_tsvector', 'pg_catalog.english', 'content');

-- name: create-document-index!
CREATE INDEX content_gin ON documents USING gin(content_tsvector);

-- name: create-queries-table!
CREATE TABLE IF NOT EXISTS queries
(
  id serial,
  query_string varchar(200),
  queried_at timestamp
  );

-- name: create-hours-table!
CREATE TABLE IF NOT EXISTS hours
(hourOfDay int UNIQUE);
  INSERT INTO hours VALUES(0);
  INSERT INTO hours VALUES(1);
  INSERT INTO hours VALUES(2);
  INSERT INTO hours VALUES(3);
  INSERT INTO hours VALUES(4);
  INSERT INTO hours VALUES(5);
  INSERT INTO hours VALUES(6);
  INSERT INTO hours VALUES(7);
  INSERT INTO hours VALUES(8);
  INSERT INTO hours VALUES(9);
  INSERT INTO hours VALUES(10);
  INSERT INTO hours VALUES(11);
  INSERT INTO hours VALUES(12);
  INSERT INTO hours VALUES(13);
  INSERT INTO hours VALUES(14);
  INSERT INTO hours VALUES(15);
  INSERT INTO hours VALUES(16);
  INSERT INTO hours VALUES(17);
  INSERT INTO hours VALUES(18);
  INSERT INTO hours VALUES(19);
  INSERT INTO hours VALUES(20);
  INSERT INTO hours VALUES(21);
  INSERT INTO hours VALUES(22);
  INSERT INTO hours VALUES(23);

-- name: get-documents
SELECT * FROM documents;

-- name: get-document
SELECT * FROM documents
WHERE id = :id
LIMIT 1;

-- name: create-document!
INSERT INTO documents (content)
VALUES (:content);

-- name: create-query!
INSERT INTO queries (query_string, queried_at)
VALUES (:query_string, :queried_at);

-- name: get-report-days
SELECT DISTINCT EXTRACT(YEAR FROM queried_at) || '_'
|| EXTRACT(MONTH FROM queried_at) || '_'
|| EXTRACT(DAY FROM queried_at) AS query_date
FROM queries
ORDER BY query_date;
