(ns doc-search.routes.home
  (:require [compojure.core :refer :all]
            [doc-search.layout :as layout]
            [doc-search.util :as util]
            [doc-search.db.core :as db]))

(defn home-page []
  (layout/render
   "home.html" {:documents (db/get-documents)}))

(defn search-page [q oper type]
  (if (not-any? nil? [q oper type])
    (let [[results sql] (db/query-documents q oper type)
          res (map #(identity {:id (:id %) :text (str (:ts_headline %) " [" (:rank %) "]")}) results)]
      (layout/render "search.html" {:q q :sql sql :results res :type type :operator oper}))
    (layout/render "search.html" {:type "exact" :operator "and"})))

(defn add-document-page []
  (layout/render "new-document.html"))

(defn create-document [content]
  (db/create-document! {:content content})
  (home-page))

(defn document-page [id]
  (let [doc (->>
             (db/get-document {:id (Integer/valueOf id)})
             (first))]
    (layout/render "document.html" {:document doc})))

(defn report-page [from to granularity]
  (if (not-any? nil? [from to granularity])
    (let [[columns results] (db/get-query-report from to granularity)]
      (layout/render "report.html" {:from from :to to :granularity granularity :columns columns :results results}))
    (layout/render "report.html" {:granularity "hour"})))

(defroutes home-routes
  (GET "/" [] (home-page))
  (GET "/new-document" [] (add-document-page))
  (POST "/new-document" [content] (create-document content))
  (GET "/search" [q operator type] (search-page q operator type))
  (GET "/documents/:id" [id] (document-page id))
  (GET "/report" [from to granularity] (report-page from to granularity)))
