(ns doc-search.util
  (:require [noir.io :as io]
            [markdown.core :as md]
            [clojure.string :as str]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-time.coerce :as c]))

(defn md->html
  "reads a markdown file from public/md and returns an HTML string"
  [filename]
  (md/md-to-html-string (io/slurp-resource filename)))

(defn contains-char? [the-string, the-char]
  (some #(= the-char %) the-string))

(defn tokenize [q]
  (->>
   (str/split q #"\"|'")
   (map str/trim)
   (filter not-empty)))

(def date-formatter (f/formatters :date-time))

(defn date-time->string [d]
  (f/unparse date-formatter d))

(defn string->date-time [s]
  (if (nil? s)
    nil
    (f/parse date-formatter s)))

(defn current-date []
  (t/now))

(defn current-timestamp []
  (c/to-timestamp (current-date)))

(defn to-sql-timestamp [d]
  (f/unparse (f/formatters :mysql) d))

(defn start-of-day-timestamp [d]
  (->> (.withTime d 00 00 00 000)
       (to-sql-timestamp)))

(defn end-of-day-timestamp [d]
  (->> (.withTime d 23 59 59 999)
       (to-sql-timestamp)))

(defn from-date-string [s]
  (if (nil? s)
    nil
    (->
     (f/formatters :date)
     (f/parse s))))

(defn pretty-date [d]
  (if (nil? d)
    "?"
    (f/unparse (f/formatter "dd/MM/yyyy") d)))

(defn prepend-last [coll]
  (cons (last coll) (butlast coll)))
